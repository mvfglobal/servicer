# Middleware
Read the [introduction](./introduction.md) first.

Standard onion based middleware support is provided as well.

## Architecture
Middleware can be set at two levels, global or action. Global middleware are triggered for every `Event` before `Event Map`. Action middleware are triggered after `Event Map` and before `Action`.

![middleware1](./images/middleware1.png)

Any number of middleware can be specified at both levels. In the middleware function you can yield `next` [effect](./effects.md) to transfer control to the next `Middleware`, `Event Map` or `Action` in the chain.

![middleware2](./images/middleware2.png)

In the above diagram you see a scenario where we have a middleware

* `preconditions` will be executed first, then
* if we yield `next` [effect](./effects.md) in this middleware the `Next Middleware | EventMap | Action` is executed, once the next operation completes
* the control is returned to our middleware where `postconditions` are executed and our middleware completes. 

In practice, middleware is just an [action](./actions.md) this is because the function signature of a middleware is identical to that of an `Action` so the below is a more accurate representation of a potential system.

global `CamelCaseArgumentKeysAction` middleware would look something like

![middleware3](./images/middleware3.png)

or action specific `CamelCaseArgumentKeysAction` middleware would look something like

![middleware4](./images/middleware4.png)

## Usage

TODO