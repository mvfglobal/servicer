# Action Body
This is where the input data for the action can be found. The source of the body, that is where the data is coming from, differ depending on the event source you are using, to read more choose your event source:

* [express-api](https://bitbucket.org/mvfglobal/servicer-express/src/master/docs/expressApiActionBody.md)
* [express-exec](https://bitbucket.org/mvfglobal/servicer-exec/src/master/docs/execActionBody.md)
* [express-sqs](https://bitbucket.org/mvfglobal/servicer-sqs/src/master/docs/sqsActionBody.md)
* [express-apollo](https://bitbucket.org/mvfglobal/servicer-apollo/src/master/docs/apolloActionBody.md)
