```mermaid
stateDiagram-v2
    Apollo : Apollo Source
    EventMapOrMiddleware1 : Middleware A1 ... An | Event Map
    ActionOrMiddleware1 : Middleware B1 ... Bn | Action 1

    state Application {
      [*] --> Apollo
      Apollo --> EventMapOrMiddleware1
      EventMapOrMiddleware1 --> ActionOrMiddleware1 : Event 1
    }
```
