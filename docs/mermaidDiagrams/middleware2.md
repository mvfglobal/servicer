```mermaid
stateDiagram-v2
    EventMap1 : Next Middleware | EventMap | Action

    state Middleware {
        [*] --> preconditions
        preconditions --> EventMap1
        EventMap1 --> postconditions
        postconditions --> [*]
    }
```
