```mermaid
stateDiagram-v2
    Apollo : Apollo Source
    EventMap1 : Event Map
    Action1 : Actions

    Sqs : Sqs Source
    EventMap2 : Event Map
    Action2 : Actions

    Other : ...
    EventMap3 : Event Map
    Action3 : Actions

    state Application {
      [*] --> Apollo
      Apollo --> EventMap1
      EventMap1 --> Action1

      [*] --> Sqs
      Sqs --> EventMap2
      EventMap2 --> Action2

      [*] --> Other
      Other --> EventMap3
      EventMap3 --> Action3
    }
```
