```mermaid
stateDiagram-v2
    Apollo : Apollo Source
    Action1 : CamelCaseArgumentKeysAction
    EventMap : Event Map
    Action2 : GetUsersAction

    state Application {
      [*] --> Apollo
      Apollo --> EventMap
      EventMap --> Action1 : USERS_REQUESTED
      Action1 --> Action2
    }
```
