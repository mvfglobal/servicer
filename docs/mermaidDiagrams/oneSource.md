```mermaid
stateDiagram-v2
    Apollo : Apollo Source
    EventMap1 : Event Map
    Action1 : Action 1
    Action2 : Action ...
    Action3 : Action n

    state Application {
      [*] --> Apollo
      Apollo --> EventMap1
      EventMap1 --> Action1 : Event 1
      EventMap1 --> Action2 : Event ...
      EventMap1 --> Action3 : Event n
    }
```
