```mermaid
stateDiagram-v2
    Apollo : Apollo Source
    Action1 : CamelCaseArgumentKeysAction
    EventMap : Event Map
    Action2 : GetUsersAction

    state Application {
      [*] --> Apollo
      Apollo --> Action1
      Action1 --> EventMap
      EventMap --> Action2 : USERS_REQUESTED
    }
```
