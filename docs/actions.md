# Actions
An `action` is a generic unit of logic that implements some behavior, you can also thing of it as a `vertical slice` in the `vertical slice architecture`.

To create an action you need to implement an action interface, e.g.

```ts
import { Effect, Action, IPayload } from '@mvf/servicer';

type ActionType = Action<IPayload, any, Effect.INothing>;
```

An `Action<..., ..., ...>` type describes the contract that the `action` function must comply with. It is a generic type with 3 inputs where:

* __Input 1__: Describes the type of the [action body](./actionBody.md), if nothing is provided [IPayload](./IPayload.md) type is used.
* __Input 2__: Describes the type of the [action headers](./actionHeaders.md), if nothing is provided [IPayload](./IPayload.md) type is used.
* __Input 3__: Describes the list of accepted [EffectTypes](./effects.md) that can be yielded in the action, if nothing is provided `void` effect will be used meaning nothing can be yielded in the action.

The above type can then be used to create the following action function.

```ts
import { nothing } from '@mvf/servicer';

const action: ActionType = async function* postMeasurements({ body, headers }) {
  // implement behavior here

  yield nothing();
}
```

Action will always have `payload` provided to them, which contains `body` and `headers`. If we look at our defined `ActionType` we can see that `body` has the type of [IPayload](./IPayload.md) and `headers` have the type of `any`.

Actions can also yield [effects](./effects.md), in this case the only effect that our action can `yield` is `nothing` because that is what we specified in our `ActionType`. You can also `yield` multiple effects using type union, e.g.

```ts
import { Effect, Action, IPayload } from '@mvf/servicer';

interface ISomeActionOutputStructure {
  firstName: string;
  lastName: string;
}

type Effects = Effect.INothing | Effect.IOutput<ISomeActionOutputStructure>;
type ActionType = Action<IPayload, any, Effects>;
```

then in your action you would be able to `yield` multiple effects `nothing` and `output` in this case, e.g. as well as `yield output({ firstName: 'Bob', lastName: 'Smith'})` 

```ts
import { nothing } from '@mvf/servicer';

const action: ActionType = async function* postMeasurements({ body, headers }) {
  if (body.firstName && body.lastName) {
    yield output({
      firstName: body.firstName,
      lastName: body.lastName,
    });
  } else {
    yield nothing();
  }
}
```

Note that output must have the right structure. 

## Recommendations

* All actions should be stored in the `Actions` folder.
* Do not organize actions by domains.
* Each action should have it's own folder where the name of the folder describes the behavior that the action implements.
* The content of the specific action's folder should follow standard `NodeJS` practices, so avoid god-files. Instead break up files into smaller files if it makes sense but importantly keep everything in the action's folder, basically there should be no global helpers with business logic, all business logic should live in one of the actions.
