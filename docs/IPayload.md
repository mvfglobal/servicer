# IPayload
A default structure for an object
```ts
export interface IPayload {
  readonly [key: string]: IPayload | any | unknown;
}
```
