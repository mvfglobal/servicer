# Introduction

Senvicer is a package designed to introduce architectural boundary between the most common javascript tools and application behavior. This package uses 3 simple concepts to do all work:

* [Actions](./actions.md)
* [EventSources and Events](./EventsSources.md)

## Core Principles

### Consistency

Application setup should be consistent for all services. Engineers should not have freedom to decide how the application is setup, and I mean, when making a new service the way the server handles logging, errors, the structure of response body, how the server starts up, or how the logged messages are formatted should not change just because a different engineer worked on a service.

### Centralization

Application setup should be centralized for all services. This is necessary in order for us to be able to change setup in a single place, aka the servicer package, so that we could distribute that change to all services easily. For example, if we want to add tracing to our services, we should not have to add tracing functionality to each individual service, we should be able to just update the servicer packages, and our services would just pull in this new functionality.

### Orthogonality

There should only be one way to do one type of a thing. Fundamentally, there is no functional difference between controller method, command handler, job handler, event handler, etc. These are all operations that just do some work.

There is a semantic difference, that is, a command would be executed on a cli, a controller method would be triggered by an http request so separating operations in this way tells you how your application works, however, in my opinion that is a useless separation.

knowing how your application works has little to no value, it is far more useful to know what your application does. For this reason all operations are just [actions](./actions.md) where each action is named in a way that makes the purpose of it clear, an action can be though of as a `vertical slice` in the `vertical slice architecture`.

### Simplicity

It should be possible to teach how all components work in a single pairing session. This principle requires us to limit package responsibilities, it means we deliberately choose to not add things that are nice but don't matter. There shouldn't be 30 different components that have to be explained and understood. At the moment if you know how [event sources](./EventSources.md), [events](./EventSources.md), [actions](./actions.md) and [middlewares](./middleware.md) work you know everything there is to know about servicer, and ideally this list should not grow.
