# Event Sources
First, `event sources` have nothing to do with `event sourcing`, instead the meaning of event source should be taken literarily. Event Sources are literal sources of events, for example, an express server is an event source.

## Architecture

Application is composed out of `EventSources`, in the diagram below we have `Apollo Source` as our event source. `Apollo Source` will produce events every time it receives requests. The `Event Map` is used to find the right `Action` based on the `Event` received. If there is an `Event` mapping to `Action` then that action will be executed with the parameters of the request.

![oneSource](./images/oneSource.png)

Each application can have multiple event sources with their own event mappings and actions.

![multipleSources](./images/multipleSources.png)

## Event Map

This is a simple object where the key of the object is an `Event` and the value is an `Action`. These Objects are used to associate `Events` to `Actions` and is used by the package to route work, e.g.

```ts
const ExpressEventSource = {
  GET_USER_REQUESTED: getUserAction
}
```

If an express integration is used an express route can be created that effectively dispatches `GET_USER_REQUESTED` event which will then be used to call `getUserAction` with request parameters.

## Events

These are constants used to uniquely identify events.

## Event Sources

Multiple `Event Source` objects can sometime be grouped into `Event Sources`, which is just another object, e.g.

```ts
const ExpressEventSources = {
  QUERIES: ExpressEventSource
}
```

This can be used to namespace your actions, but is usually not required.

## Setup

Choose your event source to understand more about the setup.

* [express-api](https://bitbucket.org/mvfglobal/servicer-express/src/master/docs/expressEventSource.md)
* [express-exec](https://bitbucket.org/mvfglobal/servicer-exec/src/master/docs/execEventSource.md)
* [express-sqs](https://bitbucket.org/mvfglobal/servicer-sqs/src/master/docs/sqsEventSource.md)
* [express-apollo](https://bitbucket.org/mvfglobal/servicer-apollo/src/master/docs/apolloEventSource.md)
