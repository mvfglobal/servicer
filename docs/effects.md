# Effects
An `effect` can be though of as a notification that can be raised (or yielded) by an action, this notification can then be interpreted by the [event source](./EventSources.md) to perform some operation.

## Effect functions
When you want to raise an `effect` in an `action` you are actually yielding one of these functions. These functions can be imported from `@mvf/servicer` package, e.g.
```ts
import { output, nothing, next } from '@mvf/servicer';
```
`output` effect function describes the output of the action, this function takes one argument which is an object that should be outputted, e.g.
```ts
yield output({
  a: 'some value',
  b: 'some other value',
});
```

`nothing` effect function describes the output of the action where you want to explicitly notify the [event source](./EventSources.md) that nothing should be returned, this function takes no arguments, e.g.

```ts
yield nothing();
```

`next` effect function is only used in the [middleware actions](./middleware.md) to transfer the execution to the next middleware in the chain. This function expects `payload` to be provided as an argument.

```ts
const middleware = async function* someMiddleware(payload) {
  // do stuff before going to next middleware in the chain
  yield next(payload);
  // do stuff after next middleware in the chain completed
}
```

## Effect types
For an `effect` to be yielded in an [action](./actions.md) you need to type it with an appropriately effect type, an `effect` that is not specified in the [action's](./actions.md) type cannot be yielded. Each `effect` has a corresponding effect type.

`output` effect is typed with `Effect.IOutput` type, e.g.
```ts
import { Effect, Action, IPayload } from '@mvf/servicer';

interface ISomeActionOutputStructure {
  firstName: string;
  lastName: string;
}

type Effects = Effect.IOutput<ISomeActionOutputStructure>;
type ActionType = Action<IPayload, IPayload, Effects>;
```

`nothing` effect is typed with `Effect.INothing` type, e.g.
```ts
import { Effect, Action, IPayload } from '@mvf/servicer';

type ActionType = Action<IPayload, IPayload, Effect.INothing>;
```

`next` effect is typed with `Middleware.INext` type, e.g.
```ts
import { Middleware, Action, IPayload } from '@mvf/servicer';

type ActionType = Action<IPayload, IPayload, Middleware.INext>;
```

## Effect behavior
Exactly what happens when an effect is yielded depends on the [event source](./EventSources.md), choose the specific doc to read more:

* [express-api](https://bitbucket.org/mvfglobal/servicer-express/src/master/docs/expressApiEffects.md)
* [express-exec](https://bitbucket.org/mvfglobal/servicer-exec/src/master/docs/execEffects.md)
* [express-sqs](https://bitbucket.org/mvfglobal/servicer-sqs/src/master/docs/sqsEffects.md)
* [express-apollo](https://bitbucket.org/mvfglobal/servicer-apollo/src/master/docs/apolloEffects.md)
