> Migrated to monorepo https://bitbucket.org/mvfglobal/rhino-monorepo/src/master/packages/servicer/

# Servicer package #

## Usage

* [Introduction](./docs/introduction.md)
* [express-api](https://bitbucket.org/mvfglobal/servicer-express/src/master/README.md)
* [express-apollo](https://bitbucket.org/mvfglobal/servicer-apollo/src/master/README.md)
* [express-exec](https://bitbucket.org/mvfglobal/servicer-exec/src/master/README.md)
* [express-sqs](https://bitbucket.org/mvfglobal/servicer-sqs/src/master/README.md)

### To install the package
Run `npm install @mvf/servicer`

## Contributing

### Setup
- Run `make` to build the container
- Run `make shell` to enter the container
- Run `npm install` to install dependencies

Refer to package.json for commands

### After merging
After you have merged a PR to master, you need to rebuild and publish your changes.

1. Checkout master `git checkout master && git pull`
2. Use one of the following make publish commands to publish changes:
    - `make publish kind=patch` - Use this if your change is a bug fix and is backwards compatible.
    - `make publish kind=minor` - Use this if your change adds new functionality and is backwards compatible.
    - `make publish kind=major` - Use this if your change is not backwards compatible.

## Setup Tests

- In RestEvents, CommandEvents or BackendEvents, add import `bindSimulator`:
    - `import { bindSimulator } from '@mvf/servicer';`
- Then use `bindSimulator` to create a `simulate` function:
    - `export const simulate = bindSimulator(RestEvents);`
- In the tests it is then possible to call actions using the simulate function:
  `const result = await simulate(ActionConstant, { headers }, { body });`

### Success response
```ts
const result = await simulate(ActionConstant, { headers }, { body });
result.output.output; // will contain response from Action
```

### Error response

#### Response with Status code & Error message
```ts
const err = await catchAsyncError(async () => {
  await simulate(ActionConstant,{headers},{body});
});

expect(err.status).toBe(400);
expect(err.message).toBe('Error message');
```
#### Response with type of error
```ts
await expect(simulate(ActionConstant, {headers}, {body})).rejects.toThrow(BadRequestError);
```