.DEFAULT_GOAL := up

.PHONY: up
up:
	$(MAKE) down
	docker-compose up -d
	$(MAKE) logs

.PHONY: down
down:
	docker-compose down --remove-orphans
	$(MAKE) _clean

.PHONY: build
build:
	$(MAKE) down
	docker-compose build
	$(MAKE) up

.PHONY: shell
shell:
	docker exec -it servicer sh

.PHONY: test
test:
	docker exec -it servicer sh -c "npm test -- -t '$(filter)'"

.PHONY: logs
logs:
	docker-compose logs -f --tail=100

.PHONY: run
run:
	docker exec -it servicer node -r source-map-support/register dist/$(target).js

.PHONY: _clean
_clean:
	docker volume rm servicer_dist || true
	rm -fr dist || true

.PHONY: publish
publish:
	rm -rf ./dist/
	npm run compile && npm test 
	npm version $(kind)
	cp ./package.json ./dist
	npm publish ./dist && rm -fr ./dist
	git push && git push --tags
