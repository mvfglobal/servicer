import Argv from '../Argv';
import { IPayload } from '../Action';

export interface ICommand {
  command(args: IPayload): Promise<void>;
  setup(program: Argv): Argv;
  getName(): string;
}
