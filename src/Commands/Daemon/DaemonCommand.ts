import Argv from '../../Argv';
import { ICommand } from '../ICommand';
import { IDaemon } from '../IDaemon';

class DaemonCommand implements ICommand {
  private name: string;

  constructor(private daemon: IDaemon) {
    this.name = `${daemon.getName()}-daemon`;
  }

  setup(program: Argv): Argv {
    return program.command(this.name, 'Starts daemon');
  }

  command = async (): Promise<void> => {
    await this.daemon.start();
  };

  getName = (): string => this.name;
}

export default DaemonCommand;
