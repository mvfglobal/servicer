import { middleware, takeMiddlewareEffects } from './Middleware';
import { next, output, Effect } from './Effects';

const mds = middleware(async function* (payload) {
  const nextResult = yield next(payload);
  console.log('middleware', nextResult);
})(async function* (payload) {
  yield output(payload);
});

async function testGen() {
  const generator = takeMiddlewareEffects(mds({ headers: {}, body: {} }));
  let nextResult: IteratorResult<Effect.ITypes, void>;
  let nextValue: Effect.IOutput | undefined;

  do {
    // eslint-disable-next-line no-await-in-loop
    nextResult = await generator.next(nextValue);
  } while (!nextResult.done);

  console.log('done');
}

void testGen();
