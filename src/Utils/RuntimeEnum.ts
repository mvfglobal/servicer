import { StandardEnum } from "../Action/IEventSource";

class RuntimeEnum<T> implements StandardEnum<T> {
  [nu: number]: string;

  [id: string]: string | T;

  constructor(e: StandardEnum<T>) {
    Object.assign(this, e);
  }
}

export default RuntimeEnum;
