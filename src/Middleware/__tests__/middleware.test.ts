import middleware from '../middleware';
import { next, Effect, status, output, capture } from '../../Effects';
import takeMiddlewareEffects from '../takeMiddlewareEffects';

const makeAsyncGenerator = (...effects: Effect.ITypes[]) => {
  return async function* () {
    yield* effects;
  };
};

const headers: any = {};
const payload = { headers, body: { data: 'some data' } };

describe('generator with one middleware', () => {
  const mock = makeAsyncGenerator(next(payload));
  const action = makeAsyncGenerator();
  const generator = takeMiddlewareEffects(middleware(mock)(action)(payload));

  it('should yield effect', async () => {
    expect((await generator.next()).value).toEqual(next(payload));
  });

  it('should yield capture effect', async () => {
    expect((await generator.next()).value).toEqual(capture());
  });

  it('should return from first middleware group', async () => {
    expect((await generator.next()).value).toEqual(undefined);
  });

  it('should be done', async () => {
    expect((await generator.next()).done).toEqual(true);
  });
});

describe('generator with two middleware', () => {
  const mockA = makeAsyncGenerator(next({ headers, body: { A: 'A' } }));
  const mockB = makeAsyncGenerator(next({ headers, body: { B: 'B' } }));
  const action = makeAsyncGenerator();
  const generator = takeMiddlewareEffects(
    middleware(mockA, mockB)(action)(payload),
  );

  it('should yield effect from first middleware', async () => {
    expect((await generator.next()).value).toEqual(
      next({ headers, body: { A: 'A' } }),
    );
  });

  it('should yield effect from second middleware', async () => {
    expect((await generator.next()).value).toEqual(
      next({ headers, body: { B: 'B' } }),
    );
  });

  it('should yield capture effect', async () => {
    expect((await generator.next()).value).toEqual(capture());
  });

  it('should yield capture effect', async () => {
    expect((await generator.next()).value).toEqual(capture());
  });

  it('should return from first middleware group', async () => {
    expect((await generator.next()).value).toEqual(undefined);
  });

  it('should be done', async () => {
    expect((await generator.next()).done).toEqual(true);
  });
});

describe("generator with yield after 'next' effect in second middleware", () => {
  const mockA = makeAsyncGenerator(next({ headers, body: { A: 'A' } }));
  const mockB = makeAsyncGenerator(
    next({ headers, body: { B: 'B' } }),
    status(200),
  );
  const action = makeAsyncGenerator();
  const generator = takeMiddlewareEffects(
    middleware(mockA, mockB)(action)(payload),
  );

  it('should yield effect from first middleware', async () => {
    expect((await generator.next()).value).toEqual(
      next({ headers, body: { A: 'A' } }),
    );
  });

  it('should yield effect from second middleware', async () => {
    expect((await generator.next()).value).toEqual(
      next({ headers, body: { B: 'B' } }),
    );
  });

  it('should yield capture effect', async () => {
    expect((await generator.next()).value).toEqual(capture());
  });

  it('should yield second effect from second middleware', async () => {
    expect((await generator.next()).value).toEqual(status(200));
  });

  it('should yield capture effect', async () => {
    expect((await generator.next()).value).toEqual(capture());
  });

  it('should be done', async () => {
    expect((await generator.next()).done).toEqual(true);
  });
});

describe("generator with multiple middleware and 'action' that yields output", () => {
  const mockA = makeAsyncGenerator(
    next({ headers, body: { A: 'A' } }),
    status(200),
  );
  const mockB = makeAsyncGenerator(next({ headers, body: { B: 'B' } }));
  const action = makeAsyncGenerator(output({ data: 'TEST' }));
  const generator = takeMiddlewareEffects(
    middleware(mockA, mockB)(action)(payload),
  );

  it('should yield effect from first middleware', async () => {
    expect((await generator.next()).value).toEqual(
      next({ headers, body: { A: 'A' } }),
    );
  });

  it('should yield effect from second middleware', async () => {
    expect((await generator.next()).value).toEqual(
      next({ headers, body: { B: 'B' } }),
    );
  });

  it('should yield effect from action', async () => {
    expect((await generator.next()).value).toEqual(output({ data: 'TEST' }));
  });

  it('should yield capture effect', async () => {
    expect((await generator.next()).value).toEqual(capture());
  });

  it('should yield capture effect', async () => {
    expect((await generator.next()).value).toEqual(capture());
  });

  it('should yield second effect from first middleware', async () => {
    expect((await generator.next()).value).toEqual(status(200));
  });

  it('should be done', async () => {
    expect((await generator.next()).done).toEqual(true);
  });
});

describe('generator with composed middleware', () => {
  const mock = makeAsyncGenerator(next({ headers, body: { A: 'A' } }));
  const mockA = makeAsyncGenerator(
    next({ headers, body: { B: 'B' } }),
    status(200),
  );
  const action = makeAsyncGenerator(output({ data: 'TEST' }));
  const generator = takeMiddlewareEffects(
    middleware(mock)(middleware(mockA)(action))(payload),
  );

  it('should yield effect from first middleware', async () => {
    expect((await generator.next()).value).toEqual(
      next({ headers, body: { A: 'A' } }),
    );
  });

  it('should yield effect from second middleware', async () => {
    expect((await generator.next()).value).toEqual(
      next({ headers, body: { B: 'B' } }),
    );
  });

  it('should yield effect from action', async () => {
    expect((await generator.next()).value).toEqual(output({ data: 'TEST' }));
  });

  it('should yield capture effect from second middleware', async () => {
    expect((await generator.next()).value).toEqual(capture());
  });

  it('should yield second effect from first middleware', async () => {
    expect((await generator.next()).value).toEqual(status(200));
  });

  it('should yield capture effect from first middleware', async () => {
    expect((await generator.next()).value).toEqual(capture());
  });

  it('should return from first middleware group', async () => {
    expect((await generator.next()).value).toEqual(undefined);
  });

  it('should be done', async () => {
    expect((await generator.next()).done).toEqual(true);
  });
});

describe('generator with middleware that throws action', () => {
  const mock = async function* () {
    throw new Error('TEST');
  };
  const action = jest
    .fn()
    .mockImplementation(makeAsyncGenerator(output({ data: 'OUTPUT' })));
  const generator = takeMiddlewareEffects(middleware(mock)(action)(payload));

  it('should throw the exception', async () => {
    await expect(generator.next()).rejects.toThrow();
  });

  it('should not call the action generator', () => {
    expect(action).not.toHaveBeenCalled();
  });
});

describe('generator with composed middleware that do not yield', () => {
  const action = jest
    .fn()
    .mockImplementation(makeAsyncGenerator(output({ data: 'OUTPUT' })));
  const generator = takeMiddlewareEffects(
    middleware()(middleware()(action))(payload),
  );

  it('should yield action output', async () => {
    expect((await generator.next()).value).toEqual(output({ data: 'OUTPUT' }));
  });

  it("should yield 'next' after last action is completed", async () => {
    expect((await generator.next()).value).toEqual(undefined);
  });

  it("should yield 'next' after first action is completed", async () => {
    expect((await generator.next()).value).toEqual(undefined);
  });

  it('should be done', async () => {
    expect((await generator.next()).done).toEqual(true);
  });
});

describe('generator with many middleware where one throws exception', () => {
  const mockA = makeAsyncGenerator(next({ headers, body: { A: 'A' } }));
  const mockB = async function* () {
    throw new Error('TEST');
  };
  const mockC = jest.fn().mockImplementation(makeAsyncGenerator());
  const action = jest
    .fn()
    .mockImplementation(makeAsyncGenerator(output({ data: 'OUTPUT' })));
  const generator = takeMiddlewareEffects(
    middleware(mockA, mockB, mockC)(action)(payload),
  );

  it('should yield from first middleware', async () => {
    expect((await generator.next()).value).toEqual(
      next({ headers, body: { A: 'A' } }),
    );
  });

  it('should throw the exception', async () => {
    await expect(generator.next()).rejects.toThrow();
  });

  it('should not call the action generator', () => {
    expect(mockC).not.toHaveBeenCalled();
  });

  it('should not call the action generator', () => {
    expect(action).not.toHaveBeenCalled();
  });
});

describe('generator with mocks to inspect propagated payloads', () => {
  const mockA = jest
    .fn()
    .mockImplementation(
      makeAsyncGenerator(next({ ...payload, A: 'A' } as any)),
    );
  const mockB = jest
    .fn()
    .mockImplementation(
      makeAsyncGenerator(next({ ...payload, B: 'B' } as any)),
    );
  const action = jest
    .fn()
    .mockImplementation(makeAsyncGenerator(output({ data: 'OUTPUT' })));
  const generator = takeMiddlewareEffects(
    middleware(mockA, mockB)(action)(payload),
  );

  it("should yield 'next' from first middleware", async () => {
    expect((await generator.next()).value).toEqual(
      next({ ...payload, A: 'A' } as any),
    );
  });

  it("should yield 'next' from second middleware", async () => {
    expect((await generator.next()).value).toEqual(
      next({ ...payload, B: 'B' } as any),
    );
  });

  it("should yield 'output' from first action", async () => {
    expect((await generator.next()).value).toEqual(output({ data: 'OUTPUT' }));
  });

  it('should call the first middleware with original payload', () => {
    expect(mockA).toHaveBeenCalledWith(payload);
  });

  it('should call the second middleware with payload yielded in first middleware', () => {
    expect(mockB).toHaveBeenCalledWith({ ...payload, A: 'A' });
  });

  it('should call action with payload yielded in second middleware', () => {
    expect(action).toHaveBeenCalledWith({ ...payload, B: 'B' });
  });
});

describe("generator to test propagated payloads where last middleware does not yield 'next'", () => {
  const mockA = makeAsyncGenerator(next({ ...payload, A: 'A' } as any));
  const action = jest
    .fn()
    .mockImplementation(makeAsyncGenerator(output({ data: 'OUTPUT' })));
  const generator = takeMiddlewareEffects(middleware(mockA)(action)(payload));

  it("should yield 'next' from first middleware", async () => {
    expect((await generator.next()).value).toEqual(
      next({ ...payload, A: 'A' } as any),
    );
  });

  it("should yield 'output' from first action", async () => {
    expect((await generator.next()).value).toEqual(output({ data: 'OUTPUT' }));
  });

  it('should call action with last yielded payload', () => {
    expect(action).toHaveBeenCalledWith({ ...payload, A: 'A' });
  });
});

describe('generator with middleware that does not yield', () => {
  const mock = makeAsyncGenerator();
  const action = jest
    .fn()
    .mockImplementation(makeAsyncGenerator(output({ data: 'OUTPUT' })));
  const generator = takeMiddlewareEffects(middleware(mock)(action)(payload));

  it('should finish', async () => {
    expect((await generator.next()).done).toEqual(true);
  });

  it('should not call the action generator', () => {
    expect(action).not.toHaveBeenCalled();
  });
});

describe('generator with middleware that yield next multiple times', () => {
  const mock = makeAsyncGenerator(next(payload), next(payload));
  const action = jest
    .fn()
    .mockImplementation(makeAsyncGenerator(output({ data: 'OUTPUT' })));
  const generator = takeMiddlewareEffects(middleware(mock)(action)(payload));

  it('should yield first next effect', async () => {
    expect((await generator.next()).value).toEqual(next(payload));
  });

  it('should yield action output', async () => {
    expect((await generator.next()).value).toEqual(output({ data: 'OUTPUT' }));
  });

  it('should yield capture effect', async () => {
    expect((await generator.next()).value).toEqual(capture());
  });

  it('should throw the exception', async () => {
    expect((await generator.next()).value).toEqual(next(payload));
  });

  it('should throw the exception', async () => {
    await expect(generator.next()).rejects.toThrow('2 yields detected');
  });

  it('should not call the action generator', () => {
    expect(action).toHaveBeenCalledTimes(1);
  });
});

describe('action with middleware that is triggered multiple times', () => {
  const mock = makeAsyncGenerator(next(payload));
  const action = jest
    .fn()
    .mockImplementation(makeAsyncGenerator(output({ data: 'OUTPUT' })));
  const actionWithMiddleware = middleware(mock)(action);

  it('should trigger middleware during the first execution', async () => {
    const generator = takeMiddlewareEffects(actionWithMiddleware(payload));
    expect((await generator.next()).value).toEqual(next(payload));
    expect((await generator.next()).value).toEqual(output({ data: 'OUTPUT' }));
  });

  it('should trigger middleware during the second execution', async () => {
    const generator = takeMiddlewareEffects(actionWithMiddleware(payload));
    expect((await generator.next()).value).toEqual(next(payload));
    expect((await generator.next()).value).toEqual(output({ data: 'OUTPUT' }));
  });

  it('should call action twice', () => {
    expect(action).toHaveBeenCalledTimes(2);
  });
});
