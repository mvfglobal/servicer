import { Effect } from '../Effects/Type';
import { Action } from '../Action';

type IWrapper = (
  action: Action<any, any, Effect.ITypes>,
) => Action<any, any, Effect.ITypes>;

export default IWrapper;
