import { Middleware, Effect } from '../Effects';

const createEffectHandler = () => {
  const outputMap: Record<number, Effect.IOutput> = {};
  let currentNext = 0;

  return (effect: Effect.ITypes | Middleware.ITypes): Effect.IOutput | void => {
    if (!effect) {
      return;
    }

    switch (effect.type) {
      case Middleware.Type.NEXT: {
        currentNext += 1;
        break;
      }
      case Effect.Type.OUTPUT: {
        outputMap[currentNext] = effect;
        break;
      }
      case Middleware.Type.CAPTURE: {
        const currentOutput = outputMap[currentNext];
        currentNext -= 1;

        // eslint-disable-next-line consistent-return
        return currentOutput;
      }
      default: {
        // do nothing
      }
    }
  };
};

const takeMiddlewareEffects = async function* (
  generator: AsyncGenerator<Effect.ITypes, void, Effect.IOutput | void>,
): AsyncGenerator<Effect.ITypes, void, Effect.IOutput | void> {
  let next: IteratorResult<Effect.ITypes, void>;
  let nextValue: Effect.IOutput | void;

  const takeEffect = createEffectHandler();

  do {
    if (nextValue) {
      // eslint-disable-next-line no-await-in-loop
      next = await generator.next(nextValue);
    } else {
      // eslint-disable-next-line no-await-in-loop
      next = await generator.next();
    }

    nextValue = takeEffect(next.value);
    if (!next.done && next.value) {
      yield next.value;
    }
  } while (!next.done);
};

export default takeMiddlewareEffects;
