import IMiddleware from './IMiddleware';
import { Middleware, Effect, capture } from '../Effects';

const checkNextYields = (countNext: number) => {
  if (countNext > 1) {
    throw new Error(
      `Middleware action should yield next() effect once; ${countNext} yields detected`,
    );
  }
};

const middleware: IMiddleware = (...[precondition, ...preconditions]) => (action) =>
  async function* (payload) {
    if (!precondition) {
      yield* action(payload);
      return;
    }

    const generator = precondition(payload);
    let next: IteratorResult<Effect.ITypes, void>;
    let nextValue: unknown;
    let countOfYieldNext = 0;

    do {
      if (nextValue) {
        // eslint-disable-next-line no-await-in-loop
        next = await generator.next(nextValue);
      } else {
        // eslint-disable-next-line no-await-in-loop
        next = await generator.next();
      }

      if (!next.done && next.value) {
        nextValue = yield next.value;
      }

      if (next.value && next.value.type === Middleware.Type.NEXT) {
        countOfYieldNext += 1;
        checkNextYields(countOfYieldNext);
        yield* middleware(...preconditions)(action)(next.value.payload);
        nextValue = yield capture();
      }
    } while (!next.done);

    // checkNextYields(countOfYieldNext, true);
  };

export default middleware;
