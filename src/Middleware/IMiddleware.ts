import IWrapper from './IWrapper';
import { Effect } from '../Effects/Type';
import { Action } from '../Action';

type IMiddleware = (
  ...preconditions: Action<any, any, Effect.ITypes>[]
) => IWrapper;

export default IMiddleware;
