import { keys } from 'lodash';
import { IEventSource } from '../Action';

class InvalidEventError extends Error {
  constructor(eventName: string, eventSource: IEventSource<any, any>) {
    const eventNames = JSON.stringify(keys(eventSource));
    super(
      `Invalid event "${eventName}" provided, valid event list: ${eventNames}`,
    );
  }
}

export default InvalidEventError;
