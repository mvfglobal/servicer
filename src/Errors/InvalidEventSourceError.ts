import { keys } from 'lodash';
import { IEventSources } from '../Action';

class InvalidEventSourceError extends Error {
  constructor(eventSourceName: string, eventSources: IEventSources<any, any>) {
    const eventSourceNames = JSON.stringify(keys(eventSources));
    super(
      `Invalid event source "${eventSourceName}" provided, valid event source list: ${eventSourceNames}`,
    );
  }
}

export default InvalidEventSourceError;
