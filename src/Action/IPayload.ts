export interface IPayload {
  readonly [key: string]: IPayload | any | unknown;
}
