import { IEventSource } from './IEventSource';
import { IPayload } from './IPayload';

export interface IEventSources<
  THeaders extends IPayload = IPayload,
  TBody extends IPayload = IPayload
> {
  [key: string]: IEventSource<any, THeaders, TBody>;
}
