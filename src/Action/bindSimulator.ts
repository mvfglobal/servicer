import { Middleware, Effect } from '../Effects/Type';
import { IEventSource } from './IEventSource';
import { IPayload } from './IPayload';

type StandardEnum<T> = {
  [id: string]: T | string;
  [nu: number]: string;
};

const bindSimulator = <
  TEventSources extends IEventSource<TEvents, THeaders, TBody>,
  TEvents extends StandardEnum<unknown>,
  THeaders extends IPayload = any,
  TBody extends IPayload = any
>(
  eventSource: TEventSources,
) => async (event: keyof TEventSources, headers: THeaders, body: TBody) => {
  const generator = eventSource[event]({ headers, body });

  const result: { [key in Effect.Type]?: any } = {};
  // eslint-disable-next-line no-restricted-syntax
  for await (const next of generator) {
    if (
      !next ||
      next.type === Middleware.Type.NEXT ||
      next.type === Middleware.Type.CAPTURE
    ) {
      // eslint-disable-next-line no-continue
      continue;
    }

    result[next.type] = next.payload;
  }

  return result;
};

export default bindSimulator;
