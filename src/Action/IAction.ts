import { IPayload } from './IPayload';

export interface IAction<H extends IPayload, B extends IPayload> {
  headers: H;
  body: B;
}
