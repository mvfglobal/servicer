import { Action } from './Action';
import { IPayload } from './IPayload';
import { Effect } from '../Effects';

export type StandardEnum<T> = {
  [id: string]: T | string;
  [nu: number]: string;
};

export type IEventSource<
  TEvents extends StandardEnum<unknown>,
  THeaders extends IPayload = IPayload,
  TBody extends IPayload = IPayload
> = {
  [key in keyof TEvents]: Action<TBody, THeaders, Effect.ITypes>;
};
