import { IAction } from './IAction';
import { IPayload } from './IPayload';
import { Effect } from '../Effects';

export type Action<
  TBody extends IPayload = IPayload,
  THeaders extends IPayload = IPayload,
  TEffects extends Effect.ITypes = void
> = (
  payload: IAction<THeaders, TBody>,
) => AsyncGenerator<TEffects, void, unknown>;
