import * as yargs from 'yargs';

type Argv = yargs.Argv;

export default Argv;
