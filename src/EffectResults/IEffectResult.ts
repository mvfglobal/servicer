interface IEffectResult<E> {
  type: E;
}

export default IEffectResult;
