import { IPayload } from "../Action";
import IEffectResult from "./IEffectResult";

export namespace Payload {
  export interface INext<TOutput extends IPayload = IPayload> {
    output: TOutput;
  }

  export type IOutput = undefined

  export type INothing = undefined;

  export interface IPause {
    terminate: boolean;
  };

  export type ITypes = INext | IOutput | INothing | IPause;
}

export namespace EffectResult {
  export enum Type {
    NEXT = 'next',
    OUTPUT = 'output',
    NOTHING = 'nothing',
    PAUSE = 'pause',
  }

  export interface INext<TOutput extends IPayload = IPayload>
    extends IEffectResult<Type.NEXT> {
    payload: Payload.INext<TOutput>;
  }
  
  export interface IOutput extends IEffectResult<Type.OUTPUT> {
    payload: Payload.IOutput;
  }

  export interface INothing extends IEffectResult<Type.NOTHING> {
    payload: Payload.INothing;
  }

  export interface IPause extends IEffectResult<Type.PAUSE> {
    payload: Payload.IPause;
  }

  export type ITypes = INext | IOutput | INothing | IPause;
}

type Type = EffectResult.Type;

export default Type;
