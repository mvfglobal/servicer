import  { EffectResult } from "./Type";

const nothing = (): EffectResult.INothing => ({
  type: EffectResult.Type.NOTHING,
  payload: undefined,
});

export default nothing;
