import { IPayload } from "../Action";
import  { EffectResult } from "./Type";

const next = <T extends IPayload>(output: T): EffectResult.INext<T> => ({
  type: EffectResult.Type.NEXT,
  payload: {
    output,
  },
});

export default next;
