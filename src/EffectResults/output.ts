import  { EffectResult } from "./Type";

const output = (): EffectResult.IOutput => ({
  type: EffectResult.Type.OUTPUT,
  payload: undefined,
});

export default output;
