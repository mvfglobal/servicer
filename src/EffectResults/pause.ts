import  { EffectResult } from "./Type";

const pause = (terminate: boolean): EffectResult.IPause => ({
  type: EffectResult.Type.PAUSE,
  payload: {
    terminate
  },
});

export default pause;
