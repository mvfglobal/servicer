import yargs from 'yargs';
import { reduce, each } from 'lodash';
import Argv from './Argv';
import { ICommand } from './Commands/ICommand';

class Application {
  private program: Argv;

  private commands: { [key: string]: ICommand };

  constructor() {
    this.commands = {};
    this.program = yargs
      .usage('Usage: $0 <command> [options]')
      .demandCommand()
      .recommendCommands()
      .strict();
  }

  addCommands = (...commands: ICommand[]): void => {
    this.program = reduce(commands, this.setupCommand, this.program);
    each(commands, this.addCommand);
  };

  run = async (): Promise<void> => {
    const args = await this.program.parse();
    const command = this.commands[args._[0]];

    if (command) {
      await command.command(args);
    }
  };

  private setupCommand = (program: Argv, command: ICommand): Argv =>
    command.setup(program);

  private addCommand = (command: ICommand): void => {
    this.commands[command.getName()] = command;
  };
}

export default Application;
