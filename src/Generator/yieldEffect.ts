import { invokeMap } from "lodash";
import { EffectResult } from "../EffectResults/Type";
import { Effect, Middleware, next, nothing, output, pause } from "../Effects";

type EffectMap = {
  [Middleware.Type.NEXT]: {
    args: Parameters<typeof next>
    return: EffectResult.INext,
  };
  [Effect.Type.OUTPUT]: {
    args: Parameters<typeof output>
    return: EffectResult.IOutput,
  };
  [Effect.Type.NOTHING]: {
    args: Parameters<typeof nothing>
    return: EffectResult.INothing,
  };
  [Effect.Type.PAUSE]: {
    args: Parameters<typeof pause>
    return: EffectResult.IPause,
  };
}

async function* yieldEffect<E extends keyof EffectMap>(
  effect: E, ...args: EffectMap[E]['args']
): AsyncGenerator<unknown, EffectMap[E]['return'], Effect.ITypes> {
  const temp: any = yield invokeMap({
    [Middleware.Type.NEXT]: next,
    [Effect.Type.OUTPUT]: output,
    [Effect.Type.NOTHING]: nothing,
    [Effect.Type.PAUSE]: pause,
  }, effect, ...args)

  return temp;
};

export default yieldEffect;
