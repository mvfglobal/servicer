import { Effect, Middleware } from "../Effects";

interface EffectMap {
  [Middleware.Type.NEXT]: Middleware.INext;
  [Middleware.Type.CAPTURE]: Middleware.ICapture;
  [Effect.Type.STATUS]: Effect.IStatus;
  [Effect.Type.OUTPUT]: Effect.IOutput;
  [Effect.Type.RAW]: Effect.IRaw;
  [Effect.Type.NOTHING]: Effect.INothing;
  [Effect.Type.PAUSE]: Effect.IPause;
}

const takeEffect = <
  E extends Middleware.Type | Effect.Type,
  I extends IteratorResult<Effect.ITypes, void>,
>(
  { value }: I,
  effect: E,
): EffectMap[E] => {
  if (!value) {
    throw new Error();
  }

  if (value.type === effect) {
    return value as never;
  }

  throw new Error(
    `expected effect type '${effect}' but got type '${value.type}' instead`,
  );
};

export default takeEffect;
