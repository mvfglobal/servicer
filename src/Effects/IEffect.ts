interface IEffect<E> {
  type: E;
}

export default IEffect;
