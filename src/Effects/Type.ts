/* eslint-disable @typescript-eslint/no-namespace */
import { IAction, IPayload } from '../Action';
import IEffect from './IEffect';

export type RawType = 'css' | 'js';

export namespace Middleware {
  export enum Type {
    NEXT = 'next',
    CAPTURE = 'capture',
  }

  export interface INext extends IEffect<Middleware.Type.NEXT> {
    payload: IAction<IPayload, IPayload>;
  }

  export type ICapture = IEffect<Middleware.Type.CAPTURE>;

  export type ITypes = INext | ICapture;
}

export namespace Payload {
  export interface IStatus {
    status: number;
  }

  export interface IRaw {
    output: string;
    type: RawType;
  }

  export interface IOutput<TOutput extends IPayload = IPayload> {
    output: TOutput;
  }

  export type INothing = undefined;

  export interface IPause {
    seconds: number;
  };

  export type ITypes = IStatus | IRaw | IOutput | INothing | IPause;
}

export namespace Effect {
  export enum Type {
    STATUS = 'status',
    OUTPUT = 'output',
    RAW = 'raw',
    NOTHING = 'nothing',
    PAUSE = 'pause',
  }

  export interface IStatus extends IEffect<Type.STATUS> {
    payload: Payload.IStatus;
  }

  export interface IRaw extends IEffect<Type.RAW> {
    payload: Payload.IRaw;
  }

  export interface IOutput<TOutput extends IPayload = IPayload>
    extends IEffect<Type.OUTPUT> {
    payload: Payload.IOutput<TOutput>;
  }

  export interface INothing extends IEffect<Type.NOTHING> {
    payload: Payload.INothing;
  }

  export interface IPause extends IEffect<Type.PAUSE> {
    payload: Payload.IPause;
  }

  export type ITypes =
    | Middleware.ITypes
    | IStatus
    | IRaw
    | IOutput
    | INothing
    | IPause
    | void;
}

type Type = Middleware.Type | Effect.Type;

export default Type;
