import { Effect } from './Type';

/**
 * Delays the execution by the specified amount, it also pauses the generator
 * allowing the controller to check termination signals
 * @param delay to pause for in seconds
 * @returns
 */
const pause = (delay = 0): Effect.IPause => ({
  type: Effect.Type.PAUSE,
  payload: {
    seconds: delay,
  },
});

export default pause;
