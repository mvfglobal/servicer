import { Effect, RawType } from './Type';

/**
 * @deprecated if a webserver functionality is needed use ExpressWeb daemon, read https://bitbucket.org/mvfglobal/servicer-express/src/master/README.md for more details. 
 */
const raw = (output: string, type: RawType): Effect.IRaw => ({
  type: Effect.Type.RAW,
  payload: {
    output,
    type,
  },
});

export default raw;
