import { Effect } from './Type';

export enum Code {
  OK = 200,
  CREATED = 201,
  NO_CONTENT = 204,
  NOT_MODIFIED = 304,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403,
  NOT_FOUND = 404,
  CONFLICT = 409,
  INTERNAL_SERVER_ERROR = 500,
}

/**
 * @deprecated The method should not be used and will be removed in future releases.
 */
const status = (code: Code): Effect.IStatus => ({
  type: Effect.Type.STATUS,
  payload: {
    status: code,
  },
});

export default status;
