import { Effect } from './Type';

const nothing = (): Effect.INothing => ({
  type: Effect.Type.NOTHING,
  payload: undefined,
});

export default nothing;
