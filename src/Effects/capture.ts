import { Middleware } from './Type';

const capture = (): Middleware.ICapture => ({
  type: Middleware.Type.CAPTURE,
});

export default capture;
