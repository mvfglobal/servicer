import { IPayload } from '../Action/IPayload';
import { Effect } from './Type';

const output = <T extends IPayload>(object: T): Effect.IOutput<T> => ({
  type: Effect.Type.OUTPUT,
  payload: { output: object },
});

export default output;
