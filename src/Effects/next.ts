import { IPayload } from '../Action/IPayload';
import { Middleware } from './Type';
import { IAction } from '../Action';

const next = (payload: IAction<IPayload, IPayload>): Middleware.INext => ({
  type: Middleware.Type.NEXT,
  payload,
});

export default next;
