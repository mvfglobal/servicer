module.exports = {
  extends: [
    'airbnb-typescript/base',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended',
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.ts', '.tsx'],
      },
    },
  },
  rules: {
    'import/extensions': ['error', 'never'],
    'no-void': ['error', { 'allowAsStatement': true }],
    '@typescript-eslint/no-explicit-any': 'off',
    'func-names': 'off',
    '@typescript-eslint/require-await': 'off',
    'require-yield': 'off'
  },
  overrides: [
    {
      files: ['*index.ts'],
      rules: {
        'import/prefer-default-export': 'off'
      }
    },
    {
      files: ["**/__tests__/**/*.ts?(x)", "**/?(*.)+(spec|test).ts?(x)"],
      rules: {
        '@typescript-eslint/no-unsafe-assignment': 'off',
        '@typescript-eslint/no-empty-function': 'off'
      }
    },
    {
      files: ["**/__mocks__/**/*.ts?(x)", "**/?(*.)+(mock).ts?(x)"],
      rules: {
        '@typescript-eslint/no-empty-function': 'off'
      }
    }
  ],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: "module",
    project: './tsconfig.json',
  },
};
